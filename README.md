# Learning Mesos Activites  

These are activities related to learning about [Apache Mesos](http://mesos.apache.org/) and related family of tools.


## Installing

* DC/OS Installation
  * Google Cloud
    * Ansible:
       * https://docs.mesosphere.com/1.9/installing/oss/cloud/gce/
       * https://github.com/dcos-labs/dcos-gce
    * Install with Terraform
       * https://docs.mesosphere.com/1.11/installing/oss/cloud/gce/
   * AWS
       * https://docs.mesosphere.com/1.11/installing/oss/cloud/aws/
       * https://downloads.dcos.io/dcos/stable/1.9.6/aws.html
       * https://downloads.dcos.io/dcos/EarlyAccess/aws.html
   * Local (Vagrant)
       * https://github.com/dcos/dcos-vagrant
* Getting Started
  * https://mlafeldt.github.io/blog/getting-started-with-the-mesosphere-dcos/
* Articles, Info, Repo on Mesos
  * https://medium.com/@GetLevvel/how-to-get-started-with-apache-mesos-marathon-and-zookeeper-24fb72d76cf9
  * https://mesos.readthedocs.io/en/0.21.1/getting-started/
  * https://github.com/guenter/mesos-getting-started
  * [MiniMesos](https://www.minimesos.org/blog/)
  * Marathon:
    * https://cilium.readthedocs.io/en/v1.0/gettingstarted/mesos/
  * Chronos:
    * https://github.com/mesos/chronos/blob/master/docs/docs/getting-started.md
